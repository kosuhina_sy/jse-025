package ru.kosuhina_sy.nlmk;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.logging.Logger;

public class Serializer {
    public static final Logger logger = Logger.getLogger(Serializer.class.getName());

    public static final String FIELD_SEPARATOR = "/";

    public static final String FILE_NAME = "list.csv";

    public void serializeList(List<Object> objects) throws IllegalAccessException, IOException, IllegalArgumentException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(FILE_NAME), StandardCharsets.UTF_8));
        Class objectClass = objects.get(0).getClass();
        StringBuilder title = new StringBuilder();
        for (Field field : objectClass.getDeclaredFields()) {
            title.append(field.getName().trim().length() == 0 ? "" : field.getName());
            title.append(FIELD_SEPARATOR);
        }
        writer.write(title.toString());
        writer.newLine();
        for (Object object : objects) {
            if (objectClass != object.getClass()) {
                throw new IllegalArgumentException();
            }
            StringBuilder stringObject = new StringBuilder();

            for (Field field : objectClass.getDeclaredFields()){
                field.setAccessible(true);
                String stringField = field.get(object) == null ? "" : field.get(object).toString();
                stringObject.append(stringField);
                stringObject.append(FIELD_SEPARATOR);
            }
            writer.write(stringObject.toString());
            writer.newLine();
        }
        writer.close();
    }
}
