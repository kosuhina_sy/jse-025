package ru.kosuhina_sy.nlmk;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static ru.kosuhina_sy.nlmk.Serializer.FIELD_SEPARATOR;
import static ru.kosuhina_sy.nlmk.Serializer.FILE_NAME;

public class Main {
    public static final Logger logger = Logger.getLogger(Main.class.getName());
    public static void main(String[] args) {
        {
            Serializer serializer = new Serializer();
            List<Object> list = new ArrayList<>();
            list.add(new Person("Homer", "Simpson"));
            list.add(new Person("Marjorie", "Simpson", LocalDate.of(1989, 12, 17)));

            try {
                serializer.serializeList(list);
            } catch (IllegalAccessException | IllegalArgumentException | IOException e) {
                logger.severe(e.toString()); return;
            }
            logger.info("Данные записаны в файл " + FILE_NAME + " с разделителем \""+FIELD_SEPARATOR+"\".");
        }
    }
}
